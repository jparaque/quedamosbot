#!/usr/bin/ruby
require 'telegram/bot'
require_relative 'reader'

#TOKEN = '145058015:AAGFCgTvCfMaNtVpkQFm3ad2mmRMNiCm6tg' #Official
TOKEN = '93391507:AAG646iVWVakkhHdg4FaFaCv69q4Khq7G9c' #Test

COMMANDS = %w(/show /delete /join /leave /create /language /help)
BOT = "@QuedamosBot"
#create - New group
#delete - Delete existing group
#join - Join a group
#leave - Leave a group you have joined
#show - Show the available groups
#help - How to use QuedamosBot
#language - Choose language

reader = Reader.new

Telegram::Bot::Client.run(TOKEN) do |bot|
    bot.listen do |message|
        reader.setup(message)
        order = ""
        order = message.text.split("@")[0] if message.text
        if COMMANDS.include? order
            puts "Message received: #{message.text}"
            case order
            when '/create'
                reader.create(bot, message)
            when '/delete'
                reader.delete(bot,message)
            when '/join'
                reader.join(bot,message)
            when '/leave'
                reader.leave(bot,message)
            when '/show'
                reader.show(bot,message)
            when '/language'
                reader.set_lang(bot,message)
            when '/help'
                reader.help bot
            end
        end
        if message.reply_to_message
            reader.read_reply(bot,message)
        end
        if message.left_chat_member
            reader.left(bot,message)
        end
        if message.new_chat_member
            reader.new_participant(bot,message)
        end

    end
end
