# This class manage the different messages received by the bot and 
# interacts with the different groups.

require "json"
require "logger"
require 'securerandom'
require 'rumoji'

PWD = File.dirname(__FILE__)

class Reader
    def initialize()
        @strings = JSON.parse(File.read("#{PWD}/res/strings.json"),:symbolize_names => true)
        files = Dir["#{PWD}/res/*json"]
        @chats = Hash.new
        files.each {|x| @chats[File.basename(x,".json")] = JSON.parse(File.read(x),:symbolize_names => true)}
        @chats.each do |k,v|
            v[:joining] = []
            v[:leaving]= []
        end
        @kbhide = Telegram::Bot::Types::ReplyKeyboardHide.new(selective: true, hide_keyboard: true)
        @force = Telegram::Bot::Types::ForceReply.new(force_reply: true, selective: true)
        @log = Logger.new("#{PWD}/log.txt")
        @langs = %w(Español English)
        @codes = {"Español"=> "es", "English" => "en"}
    end

    def setup(message)
        id = message.chat.id.to_s
        newchat(message,id) if @chats[id].nil?
        @current_chat = @chats[id]
        @cstr = @strings[@current_chat[:lang].to_sym]
        @log.info("Received message from chat #{id} and user #{message.from.first_name} (#{message.from.username})")
        @log.info("    Text: #{message.text}")
    end

    def new_participant(bot,message)
        user = message.new_chat_participant.first_name
        id = message.new_chat_participant.id
        if id == 145058015
            help bot
            return
        else
            message = @cstr[:wellcome] % [@current_chat[:title],user]
            bot.api.send_message(chat_id: @current_chat[:id],text: message)
        end
    end

    def help(bot)
            bot.api.send_message(chat_id: @current_chat[:id],  text: @cstr[:intro])
    end

    def left(bot,message)
        if message.left_chat_participant.id == 145058015
            File.delete("#{PWD}/res/#{message.chat.id}.json")
        else
            message = @cstr[:goodbye] % [message.left_chat_participant.first_name,Rumoji.decode(":confused:")]
            bot.api.send_message(chat_id: @current_chat[:id],  text: message)
        end
    end

    def newchat(message,id)
        ch = Hash.new
        ch[:active_qdd] = 0
        ch[:id] = id.to_i
        ch[:title] = message.chat.title
        ch[:qdds]= Hash.new
        ch[:joining]= []
        ch[:leaving]= []
        ch[:lang]="es"
        ch[:change_lang] = false
        ff = File.open("#{PWD}/res/#{id}.json","w")
        ff.write ch.to_json
        ff.close
        @chats[id] = ch
    end

    def create(bot, message)
        @current_chat[:qdds][SecureRandom.uuid.split("-")[0]] = {owner: message.from.to_hash, 
            users: [],
            state: 1}
        self.dbupdate
        bot.api.send_message(chat_id: @current_chat[:id], reply_to_message_id: message.message_id, reply_markup: @force, text: @cstr[:insert_name])
    end

    def read_reply(bot,message)
        @log.info("Received repply with text #{message.text}")
        @log.info("Message:/n #{message.to_hash}")
        return if assess_join(bot,message)
        return if assess_leave(bot,message)
        return if assess_lang(bot,message)
        owner = message.from
        qdd = findqdd(owner)
        if qdd.nil? #Could mean that a group is going to be deleted
            todelete = nil
            @current_chat[:qdds].each do |k,v|
                todelete = k.to_sym if v[:state] == 6 && v[:name] == message.text 
            end
            puts @current_chat
            if !todelete.nil?
                @log.info("The meetup #{@current_chat[:qdds][todelete][:name]} has been chosen to be deleted")
                name = @current_chat[:qdds][todelete][:name]
                @current_chat[:qdds].delete(todelete)
                @current_chat[:active_qdd] -= 1
                @current_chat[:qdds].each do |k,v|
                    v[:state] = 5 if v[:state] == 6 && v[:owner][:id] == owner.id
                end
                bot.api.send_message(chat_id: @current_chat[:id], reply_to_message_id: message.message_id, reply_markup: @kbhide, text: @cstr[:deleted] % name)
                self.dbupdate
                return
            end
            bot.api.send_message(chat_id: @current_chat[:id], reply_to_message_id: message.message_id, reply_markup: @kbhide, text: @cstr[:no_qdd])
            return
        end

        case qdd[:state]
        when 1
            if @current_chat[:qdds].any?{|k,v| v[:name] == message.text}
                bot.api.send_message(chat_id: @current_chat[:id], reply_to_message_id: message.message_id, reply_markup: @force, text: @cstr[:name_exists])
                return
            end
            qdd[:name] = message.text
            qdd[:state] = 2
            bot.api.send_message(chat_id: @current_chat[:id], reply_to_message_id: message.message_id, reply_markup: @force, text: @cstr[:insert_descr])
            self.dbupdate
        when 2
            qdd[:descr] = message.text
            qdd[:state] = 3
            bot.api.send_message(chat_id: @current_chat[:id], reply_to_message_id: message.message_id, reply_markup: @force, text: @cstr[:insert_time])
            self.dbupdate
        when 3
            qdd[:time] = message.text
            qdd[:state] = 4
            bot.api.send_message(chat_id: @current_chat[:id], reply_to_message_id: message.message_id, reply_markup: @force, text: @cstr[:insert_max])
            self.dbupdate
        when 4
            qdd[:max] = message.text.to_i
            qdd[:state] = 5
            msg = @cstr[:done] % qdd[:name]
            @current_chat[:active_qdd] += 1
            bot.api.send_message(chat_id: @current_chat[:id], text: msg)
            self.dbupdate
        end
    end

    def assess_join(bot, message)
        return false if !@current_chat[:joining].include? message.from.id
        name = message.text
        user = message.from
        qdd = nil
        @current_chat[:qdds].each{|k,v| qdd = v if v[:name] == name}
        return false if qdd.nil? || qdd[:state] < 5
        if user_joined(qdd,user)
            @log.info("The user #{user.first_name} tried to join the group #{qdd[:name]} but he is alreadyjoined")
            bot.api.send_message(chat_id: @current_chat[:id], reply_to_message_id: message.message_id, text: @cstr[:is_joined])
            @current_chat[:joining].delete user.id
            self.dbupdate
            return true
        end
        if qdd[:users].length < qdd[:max] || qdd[:max] == -1
            qdd[:users] << user.to_hash
            bot.api.send_message(chat_id: @current_chat[:id], reply_to_message_id: message.message_id, reply_markup: @kbhide, text: @cstr[:join_success] % qdd[:name])
            @log.info("The user #{user.first_name} has joined the group #{qdd[:name]}")
            @current_chat[:joining].delete user.id
            self.dbupdate
            return true
        end
        bot.api.send_message(chat_id: @current_chat[:id], reply_to_message_id: message.message_id, reply_markup: @kbhide, text: @cstr[:join_fail])
        @log.info("The user #{user.first_name} tried to join the group #{qdd[:name]} but is already full (#{qdd[:users].length}/#{qdd[:max]})")
        @current_chat[:joining].delete user.id
        self.dbupdate
        return true
    end

    def assess_leave(bot, message)
        return false if !@current_chat[:leaving].include? message.from.id
        name = message.text
        user = message.from
        qdd = nil
        @current_chat[:qdds].each{|k,v| qdd = v if v[:name] == name}
        qdd[:users].delete user.to_hash
        bot.api.send_message(chat_id: @current_chat[:id], reply_to_message_id: message.message_id, reply_markup: @kbhide, text: @cstr[:leave_success] % qdd[:name])
        @log.info("The user #{user.first_name} has left the group #{qdd[:name]}")
        @current_chat[:leaving].delete user.id
        self.dbupdate
        return true
    end

    def join(bot,message)
        if @current_chat[:qdds].length == 0 
            bot.api.send_message(chat_id: @current_chat[:id], reply_to_message_id: message.message_id, text: @cstr[:no_active_qdd])
            return 
        end
        id= message.from.id
        @current_chat[:joining] << id
        self.dbupdate
        buttons = []
        @current_chat[:qdds].each {|k,v| buttons << [v[:name]]}
        keyboard = Telegram::Bot::Types::ReplyKeyboardMarkup.new(keyboard: buttons,one_time_keyboard: true, selective: true)
        bot.api.send_message(chat_id: @current_chat[:id], reply_to_message_id: message.message_id, reply_markup: keyboard, text: @cstr[:select_join])
    end

    def leave(bot,message)
        if @current_chat[:qdds].length == 0 
            bot.api.send_message(chat_id: @current_chat[:id], reply_to_message_id: message.message_id, text: @cstr[:no_active_qdd])
            return 
        end
        id= message.from.id
        @current_chat[:leaving] << id
        self.dbupdate
        buttons = []
        @current_chat[:qdds].each {|k,v| buttons << [v[:name]] if v[:users].any?{|x| x[:id] == message.from.id}}
        keyboard = Telegram::Bot::Types::ReplyKeyboardMarkup.new(keyboard: buttons,one_time_keyboard: true, selective: true)
        bot.api.send_message(chat_id: @current_chat[:id], reply_to_message_id: message.message_id, reply_markup: keyboard, text: @cstr[:select_leave])
    end

    def delete(bot,message)
        if @current_chat[:qdds].length == 0 
            bot.api.send_message(chat_id: @current_chat[:id], reply_to_message_id: message.message_id, text: @cstr[:no_active_qdd])
            return 
        end
        owner = message.from
        qdds = []
        @current_chat[:qdds].each do |k,v|
            if v[:owner][:id] == owner.id
                qdds << v
            end
        end
        if qdds.empty? 
            bot.api.send_message(chat_id: @current_chat[:id], reply_to_message_id: message.message_id, text: @cstr[:no_qdd])
            return
        else
            keyboard = Telegram::Bot::Types::ReplyKeyboardMarkup.new
            buttons = []
            qdds.each {|v| 
                buttons << [v[:name]]
                v[:state] = 6
            }
            keyboard.keyboard = buttons
            keyboard.one_time_keyboard = true
            keyboard.selective = true
            bot.api.send_message(chat_id: @current_chat[:id], reply_to_message_id: message.message_id, reply_markup: keyboard, text: @cstr[:select_delete])
            self.dbupdate
        end

    end

    def findqdd(owner)
        qdd = nil
        @current_chat[:qdds].each do |k,v| 
            if v[:owner][:id] == owner.id && v[:state] < 5
                qdd = v
            end
        end
        qdd
    end

    def user_joined(qdd,user)
        qdd[:users].any?{|q| q[:id] == user.id} 
    end

    def set_lang(bot,message)
            keyboard = Telegram::Bot::Types::ReplyKeyboardMarkup.new
            buttons = []
            @langs.each {|l| 
                buttons << l
            }
            keyboard.keyboard = [buttons]
            keyboard.one_time_keyboard = true
            keyboard.selective = true
            bot.api.send_message(chat_id: @current_chat[:id], reply_to_message_id: message.message_id, reply_markup: keyboard, text: @cstr[:select_lang])
            @current_chat[:change_lang] = true
            self.dbupdate
    end

    def assess_lang(bot,message)
        if @current_chat[:change_lang]
        @log.info("User #{message.from.username}, chat #{message.chat.id}, is changing language to #{message.text}")
            @log.info("User #{message.from.username}, chat #{message.chat.id}, is changing language to #{message.text}")
            @current_chat[:lang] = @codes[message.text]
            @cstr = @strings[@current_chat[:lang].to_sym]
            bot.api.send_message(chat_id: @current_chat[:id], reply_to_message_id: message.message_id, reply_markup: @kbhide, text: @cstr[:lang_success])
            @current_chat[:change_lang] = false
            self.dbupdate
            return true
        end 
        false
    end

    def show(bot,message)
        if @current_chat[:qdds].length == 0
            message = "#{@cstr[:notoshow]}"
            bot.api.send_message(chat_id: @current_chat[:id], text: message)
            return
        end
        message = "#{@cstr[:show_title]}\n\n"
        @current_chat[:qdds].each do |k,v|
            lock = ""
            lock = Rumoji.decode(":lock:") if v[:users].length == v[:max]
            message += "#{Rumoji.decode(":white_check_mark:")} #{v[:name]} #{lock}\n"
            message += "---#{Rumoji.decode(":scroll:")} #{v[:descr]}\n"
            message += "---#{Rumoji.decode(":clock7:")} #{v[:time]}\n"
            max = @cstr[:all]
            max = v[:max] if v[:max] != -1
            message += "---Max: #{max}\n"
            message += "---#{Rumoji.decode(":busts_in_silhouette:")} (#{v[:users].length} #{@cstr[:users]})\n"
            v[:users].each do |u|
                if u[:username].nil?
                    name = u[:first_name]
                else
                    name = "@"+u[:username]
                end
                message += "     #{name}\n"
            end
            message += "----------------\n\n"
        end
        bot.api.send_message(chat_id: @current_chat[:id], text: message)
    end

    def dbupdate
        ff = File.open("#{PWD}/res/#{@current_chat[:id].to_s}.json","w") 
        ff.write @current_chat.to_json
        ff.close
    end
end
